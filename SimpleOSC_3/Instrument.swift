//
//  Instrument.swift
//  SimpleOSC_3
//
//  Created by Gian on 25/09/2019.
//  Copyright © 2019 Giannino Clemente. Licensed under GPLv3. See LICENSE for more details.
//

import UIKit
import SwiftOSC


class Instrument: UIViewController {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var idStepper: UIStepper!

    @IBOutlet weak var VolSlider: UISlider!

    @IBOutlet weak var PanicBtn: UIButton!
    @IBOutlet weak var OctDownBtn: UIButton!
    @IBOutlet weak var OctUpBtn: UIButton!

    @IBOutlet weak var MoveBtn: UIButton!
    @IBOutlet weak var MoveSlider: UISlider!
    
    override func viewDidLoad() {
           super.viewDidLoad()

           // Do any additional setup after loading the view.
        
        // Force Light Mode
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
           idLabel.text = "ID: \(DeviceID)"
           idStepper.value = Double(DeviceID)
           // OUTport = 8000 + DeviceID
           // client = OSCClient(address: "255.255.255.255", port: OUTport)
        VolSlider.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        MoveSlider.transform = CGAffineTransform.init(rotationAngle: CGFloat(Double.pi / -2))
        MoveBtn.isHidden = true
       }
       
       //Change ID Stepper
       @IBAction func idStepperChanged(_ sender: Any) {
           DeviceID = Int(idStepper.value)
           idLabel.text = "ID: \(DeviceID)"
           OUTport = 8000 + DeviceID
           client = OSCClient(address: client.address, port: OUTport)
           
       }
       
       //VolChange
       @IBAction func VolSliderChange(_ sender: Any) {
           let OSCAdress = "/inst/vol"
           let message = OSCMessage(OSCAddressPattern(OSCAdress), VolSlider.value)
           client.send(message)
       }
    @IBAction func MoveSliderChange(_ sender: Any) {
        let OSCAdress = "/inst/move"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), MoveSlider.value)
        client.send(message)
    }
    
       //PANIC
       @IBAction func PanicPressed(_ sender: Any) {
           let OSCAdress = "/midipanic"
           let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
           client.send(message)
       }
       
       //Oct-
       @IBAction func OctDownPressed(_ sender: Any) {
           let OSCAdress = "/keys/oct-"
           let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
           client.send(message)
       }
       
       //Oct+
       @IBAction func OctUpPressed(_ sender: Any) {
           let OSCAdress = "/keys/oct+"
           let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
           client.send(message)
       }

    //Boletes OnOff
    @IBAction func VerdaTouchDown(_ sender: Any) {
        let OSCAdress = "/inst/push1"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    @IBAction func BlavaTouchDown(_ sender: Any) {
        let OSCAdress = "/inst/push2"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    @IBAction func RojaTouchDown(_ sender: Any) {
        let OSCAdress = "/inst/push3"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    @IBAction func GrogaTouchDown(_ sender: Any) {
        let OSCAdress = "/inst/push4"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    @IBAction func MoradaTouchDown(_ sender: Any) {
        let OSCAdress = "/inst/push5"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 1)
        client.send(message)
    }
    
        //off
    @IBAction func VerdaTouchUp(_ sender: Any) {
        let OSCAdress = "/inst/push1"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
        client.send(message)
    }
    @IBAction func BlavaTouchUp(_ sender: Any) {
        let OSCAdress = "/inst/push2"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
        client.send(message)
    }
    @IBAction func RojaTouchUp(_ sender: Any) {
        let OSCAdress = "/inst/push3"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
        client.send(message)
    }
    @IBAction func GrogaTouchUp(_ sender: Any) {
        let OSCAdress = "/inst/push4"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
        client.send(message)
    }
    @IBAction func MoradaTouchUp(_ sender: Any) {
        let OSCAdress = "/inst/push5"
        let message = OSCMessage(OSCAddressPattern(OSCAdress), 0)
        client.send(message)
    }
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
