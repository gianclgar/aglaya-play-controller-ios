
App Thinning Size Report for All Variants of SimpleOSC_3

Variant: SimpleOSC_3-314D1430-F6C6-4B74-AFD9-2A008CC56907.ipa
Supported variant descriptors: [device: iPad8,3, os-version: 12.2], [device: iPad8,6, os-version: 12.2], [device: iPhone10,6, os-version: 12.2], [device: iPhone10,1, os-version: 12.2], [device: iPhone11,2, os-version: 12.2], [device: iPad11,2, os-version: 12.2], [device: iPhone9,2, os-version: 12.2], [device: iPhone9,4, os-version: 12.2], [device: iPhone10,2, os-version: 12.2], [device: iPhone10,5, os-version: 12.2], [device: iPhone10,3, os-version: 12.2], [device: iPhone9,1, os-version: 12.2], [device: iPad8,2, os-version: 12.2], [device: iPad7,4, os-version: 12.2], [device: iPhone10,4, os-version: 12.2], [device: iPad8,4, os-version: 12.2], [device: iPad7,1, os-version: 12.2], [device: iPad8,7, os-version: 12.2], [device: iPhone9,3, os-version: 12.2], [device: iPad7,2, os-version: 12.2], [device: iPhone11,4, os-version: 12.2], [device: iPad6,4, os-version: 12.2], [device: iPhone11,6, os-version: 12.2], [device: iPad11,4, os-version: 12.2], [device: iPhone11,8, os-version: 12.2], [device: iPad8,5, os-version: 12.2], [device: iPad11,1, os-version: 12.2], [device: iPad11,3, os-version: 12.2], [device: iPad8,1, os-version: 12.2], [device: iPad6,3, os-version: 12.2], [device: iPad7,3, os-version: 12.2], and [device: iPad8,8, os-version: 12.2]
App + On Demand Resources size: 141 KB compressed, 408 KB uncompressed
App size: 141 KB compressed, 408 KB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: SimpleOSC_3-36EB1E99-47BA-4B14-BF53-7A37AB192A3F.ipa
Supported variant descriptors: [device: iPad7,2, os-version: 11.0], [device: iPhone10,4, os-version: 11.0], [device: iPad7,3, os-version: 11.0], [device: iPad6,4, os-version: 11.0], [device: iPhone9,3, os-version: 11.0], [device: iPhone9,2, os-version: 11.0], [device: iPhone10,1, os-version: 11.0], [device: iPhone9,4, os-version: 11.0], [device: iPad7,1, os-version: 11.0], [device: iPhone10,3, os-version: 11.0], [device: iPad6,3, os-version: 11.0], [device: iPhone10,6, os-version: 11.0], [device: iPhone10,2, os-version: 11.0], [device: iPad7,4, os-version: 11.0], [device: iPhone9,1, os-version: 11.0], and [device: iPhone10,5, os-version: 11.0]
App + On Demand Resources size: 2,5 MB compressed, 7,3 MB uncompressed
App size: 2,5 MB compressed, 7,3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: SimpleOSC_3-73F848DF-4584-4BE7-B8DA-E97A01BB1A79.ipa
Supported variant descriptors: [device: iPad7,2, os-version: 12.0], [device: iPhone11,8, os-version: 12.0], [device: iPad8,2, os-version: 12.0], [device: iPhone9,1, os-version: 12.0], [device: iPhone9,3, os-version: 12.0], [device: iPhone10,1, os-version: 12.0], [device: iPad8,7, os-version: 12.0], [device: iPad8,8, os-version: 12.0], [device: iPhone10,6, os-version: 12.0], [device: iPad6,4, os-version: 12.0], [device: iPhone11,4, os-version: 12.0], [device: iPhone11,6, os-version: 12.0], [device: iPad7,1, os-version: 12.0], [device: iPhone10,4, os-version: 12.0], [device: iPad8,6, os-version: 12.0], [device: iPad7,4, os-version: 12.0], [device: iPhone9,4, os-version: 12.0], [device: iPad7,3, os-version: 12.0], [device: iPad6,3, os-version: 12.0], [device: iPad8,4, os-version: 12.0], [device: iPad8,5, os-version: 12.0], [device: iPhone10,2, os-version: 12.0], [device: iPhone10,5, os-version: 12.0], [device: iPad8,3, os-version: 12.0], [device: iPad8,1, os-version: 12.0], [device: iPhone10,3, os-version: 12.0], [device: iPhone9,2, os-version: 12.0], and [device: iPhone11,2, os-version: 12.0]
App + On Demand Resources size: 2,5 MB compressed, 7,3 MB uncompressed
App size: 2,5 MB compressed, 7,3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed
